from django.conf.urls import url

from . import views

app_name = "_auth"

urlpatterns = [
    url(r'^login$', views.Login.as_view(), name='login'),
    url(r'^logout$', views.logout_view, name='logout'),
    url(r'^signup$', views.SignUp.as_view(), name='signup'),
    url(r'^activate/(?P<hash>.+)$', views.activate_user, name='activate'),

    url(r'^password-reset$', views.Reset.as_view(), name='password-reset'),
    url(r'^confirm-password-reset/(?P<hash>.+)$', views.ConfirmReset.as_view(),
        name='confirm-password-reset')
]
