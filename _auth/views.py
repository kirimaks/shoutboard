from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from _auth.forms import LoginForm, SignUpForm, ResetForm, ConfirmResetForm
from django.contrib.auth.models import User
from itsdangerous import JSONWebSignatureSerializer
from shoutboard.settings import SECRET_KEY
from django.urls import reverse
from itsdangerous import BadSignature
from django.http import Http404
# from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib import messages
import datetime
from dateutil import parser


class Login(View):
    def get(self, request):
        login_form = LoginForm()
        return render(request, "auth/login.html", {"login_form": login_form})

    def post(self, request):
        login_form = LoginForm(request.POST)

        context = dict(login_form=login_form)

        if login_form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')

            # returns None for inactive users.
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/")

        context['login_error'] = "Invalid/Not active user"
        return render(request, "auth/login.html", context)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")


class SignUp(View):
    def get(self, request):
        signup_form = SignUpForm()
        return render(request, "auth/signup.html",
                      {"signup_form": signup_form})

    def post(self, request):
        signup_form = SignUpForm(request.POST)
        context = dict(signup_form=signup_form)

        post = request.POST     # Contains submitted data.
        if signup_form.is_valid():
            user = User.objects.create_user(username=post['username'],
                                            password=post['password'],
                                            email=post['email'],
                                            first_name=post.get('first_name'),
                                            last_name=post.get('last_name'))
            user.is_active = False
            user.save()
            self.send_confirm_link(request, user)

            message = "Sending confirmation link to: {}\
 confirm your account before login".format(user.email)

            messages.add_message(request, messages.INFO, message)
            return HttpResponseRedirect(reverse("_auth:login"))

        return render(request, "auth/signup.html", context)

    def send_confirm_link(self, request, user):
        serializer = JSONWebSignatureSerializer(SECRET_KEY)
        hash = serializer.dumps({'pk': user.pk})
        path = reverse("_auth:activate", kwargs={'hash': hash})
        url = request.build_absolute_uri(path)

        message_text = 'Confirm link for [{}]: {}'.format(user.username, url)
        send_mail('Confirm email', message_text, 'info@shoutboard.co.uk',
                  [user.email], fail_silently=True)


def activate_user(request, hash):
    serializer = JSONWebSignatureSerializer(SECRET_KEY)

    try:
        data = serializer.loads(hash)

    except BadSignature:
        raise Http404

    try:
        user = User.objects.get(pk=data.get('pk'))

        if not user.is_active:
            user.is_active = True
            user.save()

            message = "Account confirmed,you can login now."
            messages.add_message(request, messages.INFO, message)
            return HttpResponseRedirect(reverse("_auth:login"))

    except User.DoesNotExist:
        pass

    raise Http404


class Reset(View):
    def get(self, request):
        reset_form = ResetForm()
        return render(request, "auth/reset.html",
                      {"reset_form": reset_form})

    def post(self, request):
        link_valid_time = 15  # Valid time for reset link (minutes).

        reset_form = ResetForm(request.POST)

        if reset_form.is_valid():
            email = request.POST['email']
            message = "Sending reset link to {}\
 it will be valid {} minutes.".format(email, link_valid_time)
            self.send_reset_link(request, email, link_valid_time)
            messages.add_message(request, messages.INFO, message)
            return HttpResponseRedirect(reverse("_auth:login"))

        return render(request, "auth/reset.html",
                      {"reset_form": reset_form})

    def send_reset_link(self, request, email, link_valid_time):
        # User is exists, it was validated by form.
        user = User.objects.get(email=email)
        url = self.get_reset_url(request, user, link_valid_time)

        message_text = 'Reset password for {}: {} \
this link is valid {} minutes.'.format(email, url, link_valid_time)
        send_mail('Reset password', message_text, 'info@shoutboard.co.uk',
                  [email], fail_silently=True)

    def get_reset_url(self, request, user, minutes):
        serializer = JSONWebSignatureSerializer(SECRET_KEY)
        data = {
            'pk': user.pk,
            'timestamp': str(datetime.datetime.utcnow() +
                             datetime.timedelta(minutes=minutes)),
        }
        hash = serializer.dumps(data)

        path = reverse("_auth:confirm-password-reset", kwargs={'hash': hash})
        url = request.build_absolute_uri(path)

        return url


class ConfirmReset(View):
    def validate_hash(self, hash):
        serializer = JSONWebSignatureSerializer(SECRET_KEY)
        try:
            data = serializer.loads(hash)

        except BadSignature:
            raise Http404

        hash_timestamp = parser.parse(data['timestamp'])

        # If hash in the past.
        if datetime.datetime.utcnow() > hash_timestamp:
            raise Http404

        return data

    def get(self, request, hash):
        self.validate_hash(hash)    # Raises Http404
        confirm_reset_form = ConfirmResetForm()
        context = {"confirm_reset_form": confirm_reset_form}
        return render(request, "auth/confirm_reset.html", context)

    def post(self, request, hash):
        data = self.validate_hash(hash)    # Raises Http404

        confirm_reset_form = ConfirmResetForm(request.POST)

        if confirm_reset_form.is_valid():
            password = request.POST['password']

            user = User.objects.get(pk=data['pk'])
            user.set_password(password)
            user.save()

            message = "Password have been changed"
            messages.add_message(request, messages.INFO, message)
            return HttpResponseRedirect(reverse("_auth:login"))

        return render(request, "auth/confirm_reset.html",
                      {"confirm_reset_form": confirm_reset_form})
