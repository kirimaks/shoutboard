from django import forms
import re
from django.contrib.auth.models import User
from django.forms import CharField, EmailField, TextInput, PasswordInput


class LoginForm(forms.Form):
    username = CharField(label='Username',
                         min_length=3,
                         widget=TextInput(attrs={
                                          'class': 'form-control',
                                          'placeholder': 'Username',
                                          'autofocus': 'autofocus'}))

    password = CharField(label="Password", min_length=3,
                         widget=PasswordInput(attrs={
                                              'placeholder': 'Password:',
                                              'class': 'form-control'}))


class SignUpForm(forms.Form):
    username = CharField(label="Username", min_length=3,
                         widget=TextInput(attrs={
                                          'class': 'form-control',
                                          'placeholder': 'Username',
                                          'autofocus': 'autofocus'}))
    password = CharField(label="Password", min_length=10,
                         widget=PasswordInput(attrs={
                                              'placeholder': 'Password:',
                                              'class': 'form-control'}))
    email = EmailField(label="Email",
                       widget=TextInput(attrs={
                                        'class': 'form-control',
                                        'placeholder': 'Email'}))
    first_name = CharField(label="First Name",
                           widget=TextInput(attrs={
                                            'class': 'form-control',
                                            'placeholder': 'First name'}))

    last_name = forms.CharField(label="Last Name",
                                widget=TextInput(attrs={
                                                 'class': 'form-control',
                                                 'placeholder': 'Last name'}))

    def clean_password(self):
        password = self.cleaned_data['password']

        if not re.search("\d+", password):
            raise forms.ValidationError("Password must contain a number")

        return password

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()

        username = cleaned_data['username']
        email = cleaned_data['email']

        if (User.objects.filter(username=username) or
           User.objects.filter(email=email)):
            raise forms.ValidationError("User or email exists")


class ResetForm(forms.Form):
    email = EmailField(label="Email",
                       widget=TextInput(attrs={
                                        'class': 'form-control',
                                        'placeholder': 'Email'}))

    def clean_email(self):
        """ Checking if there is a user with such email """

        email = self.cleaned_data['email']

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise forms.ValidationError("Email does not exist")

        return email


class ConfirmResetForm(forms.Form):
    password = CharField(label="Password", min_length=10,
                         widget=PasswordInput(attrs={
                                              'placeholder': 'Password:',
                                              'class': 'form-control'}))

    confirm = CharField(label="Password", min_length=10,
                        widget=PasswordInput(attrs={
                                             'placeholder': 'Password:',
                                             'class': 'form-control'}))

    def clean_password(self):
        password = self.cleaned_data['password']

        if not re.search("\d+", password):
            raise forms.ValidationError("Password must contain a number")

        return password

    def clean(self):
        cleaned_data = super(ConfirmResetForm, self).clean()

        password = cleaned_data['password']
        confirm = cleaned_data['confirm']

        if password != confirm:
            raise forms.ValidationError("Passwords not match")
