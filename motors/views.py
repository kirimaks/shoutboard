from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Motor


def motors_list(request):
    records = Motor.objects.order_by("-timestamp")
    paginator = Paginator(records, 12)

    page = request.GET.get('page')
    try:
        records = paginator.page(page)

    except PageNotAnInteger:    # Display first page when wrong argument.
        records = paginator.page(1)

    except EmptyPage:   # Display last page.
        records = paginator.page(paginator.num_pages)

    return render(request, "motors/list.html", {"records": records})


def show_item(request, item_pk):
    item = get_object_or_404(Motor, pk=item_pk)
    return render(request, "motors/item.html", {"item": item})
