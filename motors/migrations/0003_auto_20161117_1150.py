# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-17 11:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('motors', '0002_auto_20161117_1104'),
    ]

    operations = [
        migrations.RenameField(
            model_name='motor',
            old_name='cover_img',
            new_name='cover_img_url',
        ),
        migrations.AddField(
            model_name='motor',
            name='source_url',
            field=models.URLField(blank=True),
        ),
    ]
