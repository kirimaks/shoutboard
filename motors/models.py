from django.db import models
# from urllib.parse import quote
import requests
# from shoutboard.settings import MEDIA_ROOT
from urllib.parse import urlparse
from os.path import splitext, basename
from uuid import uuid4
import os
from django.core.files import File

from requests.exceptions import MissingSchema


# Create your models here.
class Motor(models.Model):

    # Path inside MEDIA_ROOT.
    local_path = 'motors'

    class Meta:
        db_table = "motors"

    title = models.CharField(max_length=200)

    cover_img_url = models.URLField(blank=True)
    description = models.TextField(max_length=2000, blank=True)

    model = models.CharField(max_length=200, blank=True)
    price = models.CharField(max_length=50, blank=True)
    fuel_type = models.CharField(max_length=50, blank=True)
    mileage = models.CharField(max_length=50, blank=True)
    colour = models.CharField(max_length=50, blank=True)

    source_url = models.URLField(unique=True)

    timestamp = models.DateTimeField(auto_now=True)

    local_image = models.ImageField(upload_to=local_path, null=True)

    def __str__(self):
        return self.title

    def fetch_cover_image(self):
        parsed_url = urlparse(self.cover_img_url)
        name, type = splitext(basename(parsed_url.path))

        # Very simple checking of extensions.
        # TODO: check for (png|jpg|jpeg|gif)
        try:
            assert(type)
        except:
            return "Image format error.."

        new_name = str(uuid4()) + type
        tmp_path = os.path.join("/tmp", new_name)

        # Download file.
        with open(tmp_path, "wb") as buff:
            try:
                resp = requests.get(self.cover_img_url)
                buff.write(resp.content)
            except MissingSchema:
                return "downloading error"

        # Delete old file if exists.
        if self.local_image:
            self.local_image.delete()

        # Upload to django.
        buff = File(open(tmp_path, 'rb'))
        self.local_image.save(new_name, buff)

        # Clean up (maybe delete odl).
        os.remove(tmp_path)

        return "ok"
