from django.conf.urls import url

from . import views

app_name = "motors"

urlpatterns = [
    url(r'^$', views.motors_list, name='motors_list'),
    url(r'^item/(?P<item_pk>\d+)$', views.show_item, name='show_item')
]
