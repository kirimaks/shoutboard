from django.contrib import admin
from .models import Motor


def delete_model(modeladmin, request, queryset):
    for obj in queryset:
        obj.local_image.delete()
        obj.delete()
delete_model.short_description = "Delete model with images"


class MotorAdmin(admin.ModelAdmin):
    list_display = ["title", "timestamp"]
    ordering = ["-timestamp"]
    actions = [delete_model]

# Register your models here.
admin.site.register(Motor, MotorAdmin)
