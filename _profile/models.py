from django.db import models
from django.contrib.auth.models import User
import hashlib
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class Profile(models.Model):
    class Meta:
        db_table = "profile"

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    company_name = models.CharField(max_length=100, blank=True)
    vat_number = models.CharField(max_length=100, blank=True)

    address = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    post_code = models.CharField(max_length=50, blank=True)

    @property
    def gravatar(self):
        hash = hashlib.md5(self.user.email.encode('utf-8')).hexdigest()
        url = "https://gravatar.com/avatar/{}".format(hash)
        return url


@receiver(post_save, sender=User)
def save_user(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
