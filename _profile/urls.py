from django.conf.urls import url
from . import views

app_name = "_profile"

urlpatterns = [
    url(r'^$', views.Profile.as_view(), name='profile_index'),
    url(r'password$', views.ProfilePassword.as_view(),
        name='profile_password'),
    url(r'address$', views.ProfileAddress.as_view(),
        name='profile_address'),

    url(r'avatar$', views.ProfileAvatar.as_view(),
        name='profile_avatar'),

    url(r'company$', views.ProfileCompany.as_view(),
        name='profile_company'),
]
