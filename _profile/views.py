from django.shortcuts import render
from django.views import View
# from django.urls import reverse
# from django.http import HttpResponseRedirect

from .forms import ChangePasswordForm, ChangeAddressForm, ChangeCompanyForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib import messages
from _profile.models import Profile as UserProfile


class Profile(LoginRequiredMixin, View):
    login_url = "/auth/login"

    def get(self, request):
        context = {"user": request.user}
        return render(request, "profile/index.html", context)


class ProfilePassword(LoginRequiredMixin, View):
    login_url = "/auth/login"

    def get(self, request):
        form = ChangePasswordForm()
        context = {
            "user": request.user,
            "form": form,
        }
        return render(request, "profile/password.html", context)

    def post(self, request):
        user = request.user
        form = ChangePasswordForm(data=request.POST, user=user)

        if form.is_valid():
            new_password = form.cleaned_data['new_password']
            user.set_password(new_password)
            user.save()
            messages.success(request, "Password updated")
            return redirect(reverse("_profile:profile_password"))

        context = {
            "user": request.user,
            "form": form,
        }

        return render(request, "profile/password.html", context)


class ProfileAddress(LoginRequiredMixin, View):
    login_url = "/auth/login"

    def get(self, request):
        profile = UserProfile.objects.get(user__username=request.user.username)

        form = ChangeAddressForm(initial={
                                 'address': profile.address,
                                 'city': profile.city,
                                 'country': profile.country,
                                 'post_code': profile.post_code,
                                 })
        return render(request, "profile/address.html", {"form": form})

    def post(self, request):
        profile = UserProfile.objects.get(user__username=request.user.username)
        form = ChangeAddressForm(request.POST)

        if form.is_valid():
            profile.address = form.cleaned_data['address']
            profile.city = form.cleaned_data['city']
            profile.country = form.cleaned_data['country']
            profile.post_code = form.cleaned_data['post_code']
            profile.save()

            messages.success(request, "Address updated")
            return redirect(reverse("_profile:profile_address"))

        return render(request, "profile/address.html", {"form": form})


class ProfileAvatar(LoginRequiredMixin, View):
    login_url = "/auth/login"

    def get(self, request):
        return render(request, "profile/avatar.html", {})


class ProfileCompany(LoginRequiredMixin, View):
    login_url = "/auth/login"

    def get(self, request):
        profile = UserProfile.objects.get(user__username=request.user.username)
        form = ChangeCompanyForm(initial={
                                 'company_name': profile.company_name,
                                 'vat_number': profile.vat_number,
                                 })
        return render(request, "profile/company.html", {"form": form})

    def post(self, request):
        profile = UserProfile.objects.get(user__username=request.user.username)
        form = ChangeCompanyForm(request.POST)

        if form.is_valid():
            profile.company_name = form.cleaned_data['company_name']
            profile.vat_number = form.cleaned_data['vat_number']
            profile.save()

            messages.success(request, "Updated")
            return redirect(reverse("_profile:profile_company"))

        return render(request, "profile/company.html", {"form": form})
