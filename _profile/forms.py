from django import forms
from django.forms import CharField, PasswordInput, TextInput, Textarea
import re


class ChangePasswordForm(forms.Form):
    def __init__(self, user=None, data=None):
        self.user = user
        super(ChangePasswordForm, self).__init__(data=data)

    cur_password = CharField(label="Current password", min_length=10,
                             widget=PasswordInput(attrs={
                                                  'placeholder': 'Current password:',
                                                  'class': 'form-control'}))

    new_password = CharField(label="New password", min_length=10,
                             widget=PasswordInput(attrs={
                                                  'placeholder': 'New password:',
                                                  'class': 'form-control'}))

    confirm = CharField(label="Confirm", min_length=10,
                        widget=PasswordInput(attrs={
                                             'placeholder': 'Confirm:',
                                             'class': 'form-control'}))

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()

        cur_password = cleaned_data['cur_password']
        if not self.user.check_password(cur_password):
            raise forms.ValidationError("Wrong current password")

        new_password = cleaned_data['new_password']
        confirm = cleaned_data['confirm']

        # Check for numbers in new password.
        if not re.search("\d+", new_password):
            raise forms.ValidationError("New password must contain a number")

        if new_password != confirm:
            raise forms.ValidationError("Passwords not match")


class ChangeAddressForm(forms.Form):
    address = CharField(label='Address', required=False,
                        widget=Textarea(attrs={
                                        'class': 'form-control',
                                        'placeholder': 'Address',
                                        'autofocus': 'autofocus',
                                        'rows': 5}))

    city = CharField(label='City:', required=False,
                     widget=TextInput(attrs={
                                      'class': 'form-control',
                                      'placeholder': 'City'}))

    country = CharField(label='Country:', required=False,
                        widget=TextInput(attrs={
                                         'class': 'form-control',
                                         'placeholder': 'Country'}))

    post_code = CharField(label='Postal Code:', required=False,
                          widget=TextInput(attrs={
                                           'class': 'form-control',
                                           'placeholder': 'Postal code'}))


class ChangeCompanyForm(forms.Form):
    company_name = CharField(label='Company name:', required=False,
                             widget=TextInput(attrs={
                                              'class': 'form-control',
                                              'placeholder': 'Company'}))

    vat_number = CharField(label='VAT Number:', required=False,
                           widget=TextInput(attrs={
                                            'class': 'form-control',
                                            'placeholder': 'VAT'}))
