from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^motor/(?P<pk>\d+)$', views.MotorDetailView.as_view()),
    url(r'^motors/$', views.MotorsListView.as_view()),

    url(r'^motor/source_url/(?P<source_url>.+)$',
        views.MotorsInfoView.as_view()),
]

urlpaterns = format_suffix_patterns(urlpatterns)
