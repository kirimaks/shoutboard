from django.apps import AppConfig


class Api10Config(AppConfig):
    name = 'api_1_0'
