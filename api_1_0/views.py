from motors.models import Motor

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from django.http import Http404

from api_1_0.serializers import MotorSerializer

from urllib.parse import quote


class MotorsListView(APIView):
    """
    Motors list/create.
    """

    def get_id_by_source_url(self, source_url):
        if source_url:
            try:
                obj = Motor.objects.get(source_url=source_url)
                return obj.pk

            except Motor.DoesNotExist:
                pass

        return None

    def get(self, request, format=None):
        motors = Motor.objects.all().order_by("-timestamp")
        serializer = MotorSerializer(motors, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MotorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        # Custom error with record id (maybe be used for futer patch request).
        # custom_errors = {
        #     "pk": self.get_id_by_source_url(request.data.get('source_url'))
        # }
        # custom_errors.update(serializer.errors)
        # return Response(custom_errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MotorDetailView(APIView):
    """
    Motor get/update
    """

    def get_obj_by_pk(self, pk):
        try:
            motor = Motor.objects.get(pk=pk)
        except Motor.DoesNotExist:
            raise Http404()

        return motor

    def get(self, request, pk):
        motor = self.get_obj_or_404(pk)
        serializer = MotorSerializer(motor)
        return Response(serializer.data)

    def patch(self, request, pk):
        motor = self.get_obj_by_pk(pk)
        # motor = Motor.objects.get(pk=pk)
        serializer = MotorSerializer(motor, data=request.data, partial=True)

        if serializer.is_valid():
            # data = request.data.dict()
            # motor.update(**data)
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MotorsInfoView(APIView):
    def get_obj_by_url(self, source_url):
        try:
            motor = Motor.objects.get(source_url=source_url)
        except Motor.DoesNotExist:
            raise Http404()

        return motor

    def get(self, request, source_url):
        source_url = quote(source_url, safe=':/')
        print("Searching for: [{}]".format(source_url))
        motor = self.get_obj_by_url(source_url)
        serializer = MotorSerializer(motor)
        return Response(serializer.data)
