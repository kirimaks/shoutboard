# from django.db import IntegrityError
from rest_framework import serializers
from motors.models import Motor
# from rest_framework.exceptions import APIException
# from pprint import pprint
# from urllib.parse import quote


# -------- Exceptions ----------
# class IntegrityException(APIException):
#     status_code = 400
#     default_detail = 'IntegrityError: "source_url" already exists...'
#     default_code = 'Record already exists'
# ------------------------------


class MotorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Motor
        # fields = "__all__"
        exclude = ('local_image',)

    def create(self, validated_data):
        motor = Motor.objects.create(**validated_data)
        motor.save()

        motor.fetch_cover_image()

        return motor

    def update(self, inst, valid_data):
        # inst: current instance saved in database.
        # valid_data: validated_data from request.

        # Save current source_url.
        old_cover_img_url = inst.cover_img_url

        # Write new values or leave old.
        inst.title = valid_data.get("title", inst.title)
        inst.description = valid_data.get("description", inst.description)
        inst.model = valid_data.get("model", inst.model)
        inst.price = valid_data.get("price", inst.price)
        inst.fuel_type = valid_data.get("fuel_type", inst.fuel_type)
        inst.mileage = valid_data.get("mileage", inst.mileage)
        inst.colour = valid_data.get("colour", inst.colour)
        inst.source_url = valid_data.get("source_url", inst.source_url)
        inst.cover_img_url = valid_data.get("cover_img_url", inst.cover_img_url)
        inst.save()

        # If new image url was submitted, update local image.
        if inst.cover_img_url != old_cover_img_url:
            inst.fetch_cover_image()

        return inst
